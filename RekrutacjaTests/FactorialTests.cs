using System;
using NUnit.Framework;
using RekrutacjaRobak;

namespace Tests
{
    public class FactorialTests
    {
        private readonly Factorial _service = new Factorial();

        [TestCase(0u, 1UL)]
        [TestCase(1u, 1UL)]
        [TestCase(2u, 2UL)]
        [TestCase(3u, 6UL)]
        [TestCase(4u, 24UL)]
        [TestCase(5u, 120UL)]
        [TestCase(6u, 720UL)]
        [TestCase(7u, 5_040UL)]
        [TestCase(8u, 40_320UL)]
        [TestCase(9u, 362_880UL)]
        [TestCase(10u, 3_628_800UL)]
        [TestCase(11u, 39_916_800UL)]
        [TestCase(16u, 20_922_789_888_000UL)]
        [TestCase(19u, 121_645_100_408_832_000UL)]
        [TestCase(20u, 2_432_902_008_176_640_000UL)]
        public void Factorial_TestAllowedCases(uint arg, ulong expected)
        {
            Assert.AreEqual(_service.Calculate(arg), expected);
        }

        [Test]
        public void Factorial_21_ThrowsArgumentOutOfRangeException()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => _service.Calculate(21));
        }
        
        [Test]
        public void Factorial_GetFactorial6()
        {
            Assert.AreEqual(_service.Factorial6, 720);

        }
    }
}
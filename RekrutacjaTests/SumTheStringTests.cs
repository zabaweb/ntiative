using NUnit.Framework;
using RekrutacjaRobak;

namespace Tests
{
    public class SumTheStringTests
    {
        private readonly SumTheString _service = new SumTheString();

     
        [TestCase("0 0 0",0)]
        [TestCase("-1 0 1",0)]
        [TestCase("1 sd 2",3)]
        [TestCase("33 sd 45 df ",78)]
        [TestCase("5 62 ar 11", 78)]
        public void SumTheString_ValidCaseShouldCalculateProperly(string str, int expected)
        {
            Assert.AreEqual(_service.SumTheStringMethod(str),expected);
        }
        
        [TestCase(null,0)]
        [TestCase("",0)]
        [TestCase(" ",0)]
        [TestCase("  ",0)]
        [TestCase("A B C",0)]
        
        [TestCase("T͎̥̼̻͕̥͟ọ ̛̜̠̞̻i͎̥̝̮͞ͅn͙̬͔̗͔̖͕v̧̰̻̱͇͍ok̵̫̼̩͉ͅe͖̪͙̯̫ͅ ̗̙t̥͍͢ẖe̹̟̻̺̩͓̰ ͈̹̙͇̟h̝̠̥͖i̶v̼̳̩̯̙̤͕e͟-mi̟͖͕̪̯̩n̲̰̗̱̤̦ͅd̡̮̳̦͇̮ͅ ̨̗̪r̨͈̥̞̻̯e̼̬̻̳͓̠p͓͠r͎̝͓̝̘͟es҉̙̟͚̝̘e̯͇̬̟̭̭n̰̹̞͇̼̞͜ͅt̷͙̫̬̝̠i̱n͏g̴͎͖̪͖̱ ̰̲͔̰̬͎͔c̴h̜̜̀a̧̪o̰ͅs͓̤͕̞.̮",0)]
        
        [TestCase("😁 😂 ✖ 🚅",0)]
        public void SumTheString_InvalidCaseShouldReturnZero(string str, int expected)
        {
            Assert.AreEqual(_service.SumTheStringMethod(str),expected);
        }
    }
}
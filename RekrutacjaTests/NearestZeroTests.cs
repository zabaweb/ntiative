using NUnit.Framework;
using RekrutacjaRobak;

namespace Tests
{
    public class NearestZeroTests
    {
        private static readonly object[] DataSource =
        {
            new object[] {new[] {1, 2, 3, 4, 5}, 1},
            new object[] {new[] {-1, 1, 3, 4, 5}, 1},
            new object[] {new[] {1, -1}, 1},
            new object[] {new[] {-1, 1}, 1},
            new object[] {new[] {-110, 1}, 1},
            new object[] {new[] {-1, 2, 3, 4, 5}, -1},
            new object[] {new[] {0,-1, 2, 3, 4, 5}, 0},
            new object[] {new[] {int.MinValue}, int.MinValue},
            new object[] {new[] {int.MaxValue}, int.MaxValue},
            new object[] {new[] {int.MaxValue, int.MinValue}, int.MaxValue},
            new object[] {new[] {int.MaxValue, int.MinValue + 1}, int.MaxValue},
        };

        [TestCaseSourceAttribute(nameof(DataSource))]
        public void NearestZero_TestTypicalCases(int[] array, int expected)
        {
            Assert.AreEqual(array.GetNumberNearest0(), expected);
        }
    }
}
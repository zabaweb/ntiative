using NUnit.Framework;
using RekrutacjaRobak;

namespace Tests
{
    public class NonEmptyTableTests
    {
        private static readonly object[] ValidCases =
        {
            new object[] {new[] {1, 2, 2, 3, 3}, 1},
            new object[] {new[] {1, 1, 3, 4, 4}, 3},
            new object[] {new[] {5,5,6,1,4,4,6}, 1},
            new object[] {new[] {1}, 1},
        };

        [TestCaseSource(nameof(ValidCases))]
        public void NearestZero_TestTypicalCases(int[] array, int expected)
        {
            Assert.AreEqual(array.FindOddElement(), expected);
        }
    }
}
using NUnit.Framework;
using RekrutacjaRobak;

namespace Tests
{
    public class FizBuzztTests
    {
        private readonly FizBuzz _service = new FizBuzz();
        
        [TestCase(1, "1")]
        [TestCase(3, "3 One")]
        [TestCase(5, "5 Two")]
        [TestCase(15, "15 Three")]
        public void Factorial_TestAllowedCases(int arg, string expected)
        {
            Assert.AreEqual(_service.Mapper(arg), expected);
        }
    }
}
--CREATE TABLE employee
--(
--    Id      INT NOT NULL UNIQUE ,
--    Name    NVARCHAR(30),
--    Surname NVARCHAR(30)
--);

--CREATE TABLE sales
--(
--    Id          INT NOT NULL ,
--    Employee    INT,
--    Day         DATE,
--    Sum         INT
--);


-- a
SELECT
    Day,
    SUM(Sum) as SalePerDay
FROM sales
GROUP BY Day

-- b

SELECT
    e.Id,
    COALESCE(SUM(s.Sum),0) as SalePerEmployee
FROM employee e
         LEFT JOIN sales s on s.Employee = e.Id
GROUP BY e.Id


--c
SELECT TOP 1
    *
FROM
    (SELECT
         e.Id,
         COALESCE(SUM(s.Sum),0) as SalePerEmployee
     FROM employee e
              LEFT JOIN sales s on s.Employee = e.Id
     GROUP BY e.Id ) subQuery
ORDER BY subQuery.SalePerEmployee

  
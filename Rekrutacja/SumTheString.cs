﻿using System;
using System.Linq;

namespace RekrutacjaRobak
{
    public class SumTheString
    {
        public int SumTheStringMethod(string str) =>
            Sanitize(str)
                .Split(' ')
                .Select(x => x.Trim())
                .Select(x => int.TryParse(x, out var result) ? result : 0)
                .DefaultIfEmpty(0)
                .Sum();

        private static string Sanitize(string str) => str ?? "";
    }
}
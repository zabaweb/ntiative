using System;
using System.Linq;

namespace RekrutacjaRobak
{
    public class FizBuzz
    {
        public void ConsolePrinter()
        {
            foreach (var i in Enumerable.Range(1,100))
            {
                var result = Mapper(i);

                Console.WriteLine(result);
            }
        }

        public string Mapper(int number)
        {
            var div3 = number % 3 == 0;
            var div5 = number % 5 == 0;
            
            if (div3 && div5)
            {
                return $"{number} Three";
            }
            if (div3)
            {
                return $"{number} One";
            }
            
            if (div5)
            {
                return $"{number} Two";
            }

            return $"{number}";
        }

    }
}
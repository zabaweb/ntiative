using System.Linq;

namespace RekrutacjaRobak
{
    public static class NonEmptyTable
    {
        /// <param name="table">A non-empty table A is given consisting of N integers</param>
        /// <returns></returns>
        public static int FindOddElement(this int[] table) =>
            table.GroupBy(x => x).Single(x => x.Count<int>() == 1).Single();
    }
}
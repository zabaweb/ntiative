using System;

namespace RekrutacjaRobak
{
    public class Factorial
    {
        public int Factorial6 => (int)Calculate(6);
        
        public ulong Calculate(uint arg)
        {
            if (arg > 20)
            {
                throw new ArgumentOutOfRangeException(nameof(arg),"Cannot calculate factorial for number greater than 20");
            }

            if (arg == 0)
            {
                return 1;
            }

            ulong buff = 1;

            for (ulong i = 1; i <= arg; i++)
            {
                buff *= i;
            }

            return buff;
        }
    }
}

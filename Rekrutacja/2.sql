CREATE TABLE employee
(
    Id      INT NOT NULL PRIMARY KEY,
    Name    NVARCHAR(30),
    Surname NVARCHAR(30),
    Manager_id int
);

CREATE TABLE manager
(
    Id          INT NOT NULL  PRIMARY KEY,
    Identity_id  INT NOT NULL,
    FOREIGN KEY (Identity_id) REFERENCES employee(Id)
);

  
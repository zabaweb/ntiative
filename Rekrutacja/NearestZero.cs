using System;
using System.Linq;

namespace RekrutacjaRobak
{
    public static class NearestZero
    {
        public static int GetNumberNearest0(this int[] arr)
        {
            var buffer = int.MinValue;
            var abs = int.MaxValue;
            foreach (var i in arr)
            {
                // Small speedup
                if (i == 0)
                {
                    return 0;
                }

                // Prevent for  Math.Abs(int.MinValue)
                if (i == int.MinValue)
                {
                    continue;
                }
                
                var newAbs = Math.Abs(i);
                if (newAbs > abs)
                {
                    continue;
                }

                if (newAbs < abs)
                {
                    Reassign();
                    continue;
                }

                if (newAbs == abs && i > buffer)
                {
                    Reassign();
                }

                void Reassign()
                {
                    abs = newAbs;
                    buffer = i;
                }
            }

            return buffer;
        }
    }
}